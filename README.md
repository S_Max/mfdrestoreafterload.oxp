MFD Restore After Load

By S Max

Restore MFD configuration after game loading.

Available in [Oolite](http://www.oolite.org/) package manager (HUDs).

License
=======
This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

Version History
===============

0.1

- Initial release

