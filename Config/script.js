this.name           = "MFDRestoreAfterLoad";
this.author         = "SMax";
this.copyright      = "2015 SMax";
this.licence        = "CC-BY-NC-SA 4.0";
this.description    = "Restore MFD configuration after game loading.";
this.version        = "0.1 alpha 1";

"use strict";

this.startUpComplete = function() {
	var config = JSON.parse(missionVariables.MFDRestoreAfterLoad_Data);

	if (config && config.length > 0) {
		var p = player.ship;	
		for (var i = 0; i < config.length; i++) {
			try {
				if (config[i]) {
					p.setMultiFunctionDisplay(i, config[i]);
				}
			}
			catch (err) {
				log(this.name, "ERROR: Unable to set MFD " + (i + 1) + " to " + config[i]);
			}
		}
	}
}

this.playerWillSaveGame = function() {
	var p = player.ship;
	var config = [];

	if (p) {
		for (var i = 0; i < p.multiFunctionDisplayList.length; i++) {
			config.push(p.multiFunctionDisplayList[i]);
		}
	}
	
	missionVariables.MFDRestoreAfterLoad_Data = JSON.stringify(config);
}